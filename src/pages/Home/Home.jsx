import { Component } from 'react'
import Modal from "../../components/Modal";
import { modalProps } from '../../components/Modal/modalProps.jsx'
import Header from '../../components/Header';
import ProductList from '../../components/ProductList';
import { getDataFromLS } from '../../utils';

export default class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modalOpen: null,
            favorites: [],
            cart: [],
            artNum: null,
            products: [],
            error: null,
        }
    }

    componentDidMount() {
        fetch('./products.json')
            .then(res => res.json())
            .then(
                (result) => { this.setState(state => ({ ...state, products: result })) },
                (error) => { this.setState(state => ({ ...state, error })) }
            )

        this.setState(state => ({
            ...state,
            favorites: getDataFromLS('favorites'),
            cart: getDataFromLS('cart')
        }))
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.favorites !== this.state.favorites) {
            localStorage.setItem('favorites', JSON.stringify(this.state.favorites))
        } else if (prevState.cart !== this.state.cart) {
            localStorage.setItem('cart', JSON.stringify(this.state.cart))
        }
    }

    openModalHandler = (e, type, artNum) => {
        e.preventDefault();

        this.setState(state => ({ ...state, modalOpen: type, artNum }))
    }

    closeModalHandler = () => this.setState(state => ({ ...state, modalOpen: null, artNum: null }))

    addToFavoriteHandler = (e, product) => {
        e.preventDefault();

        this.state.favorites.find(favorite => favorite.artNum === product.artNum)
            ? this.setState(state => ({ ...state, favorites: [...state.favorites.filter(favorite => favorite.artNum !== product.artNum)] }))
            : this.setState(state => ({ ...state, favorites: [...state.favorites, product] }))
    }

    addToCartHandler = () => {
        const newCart = this.state.products.find(product => product.artNum === this.state.artNum)

        this.setState(state => ({ ...state, cart: [...state.cart, newCart] }))
        this.closeModalHandler()
    }

    render() {
        return (
            <>
                {this.state.modalOpen
                    && <Modal
                        onSubmit={this.addToCartHandler}
                        onCancel={this.closeModalHandler}
                        data={modalProps.find(modal => modal.type === this.state.modalOpen)} />}
                <Header
                    favorites={this.state.favorites.length}
                    cart={this.state.cart.length} />
                {!this.state.error
                    ? <ProductList
                        products={this.state.products}
                        addToFavorite={this.addToFavoriteHandler}
                        favorites={this.state.favorites}
                        addToCart={this.openModalHandler} />
                    : <div>Відбулась помилка завантаження списку товарів, спробуйте оновити сторінку</div>
                }
                {/* {!this.state.error && !this.state.products.length
                    ? <div>Завантаження...</div>
                    : this.state.error
                        ? <div>Відбулась помилка завантаження списку товарів, спробуйте оновити сторінку</div>
                        : <ProductList
                            products={this.state.products}
                            addToFavorite={this.addToFavoriteHandler}
                            favorites={this.state.favorites}
                            addToCart={this.openModalHandler} />
                } */}
            </>
        )
    }
}