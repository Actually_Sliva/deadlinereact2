import { Component } from 'react'
import PropTypes from 'prop-types';

export default class Button extends Component {

    render() {

        const { onClick, className, type, text, children } = this.props

        return (
            <button
                onClick={onClick}
                className={className}
                type={type}
            >
                {text}
                {children}
            </button>
        )
    }
}

Button.propTypes = {
    onClick: PropTypes.func,
    className: PropTypes.string,
    type: PropTypes.string,
    text: PropTypes.string,
    children: PropTypes.object,
}
Button.defaultProps = {
    type: 'button',
}