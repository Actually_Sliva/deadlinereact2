import { Component } from "react"
import ProductItem from "../ProductItem"
import style from './productList.module.scss'
import PropTypes from 'prop-types';

export default class ProductList extends Component {

    render() {
        const { products, addToFavorite, favorites, addToCart } = this.props
        return (
            <div className={style.productList}>
                <div className={style.productList__container}>
                    <div className={style.productList__wrapper}>
                        {products.map(product => 
                            <ProductItem
                                key={product.artNum}
                                product={product}
                                onAddToFavorite={addToFavorite}
                                favorites={favorites} 
                                onAddToCart={addToCart} />
                        )}
                    </div>
                </div>
            </div>
        )
    }
}

ProductList.propTypes = {
    addToFavorite: PropTypes.func,
    favorites: PropTypes.array,
    addToCart: PropTypes.func,
    product: PropTypes.array
}